package jephthia.evaluator;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import exceptions.DivideByZeroException;
import exceptions.InvalidOperandException;
import exceptions.InvalidParenthesisException;
import exceptions.MisplacedOperandException;
import exceptions.MisplacedOperatorException;

import org.junit.Test;

@RunWith(Parameterized.class)
public class AppTest
{
	private MyQueue<String> infix;
	private double expectedResult;
	
	/**
	 * @param infix The infix that we are currently testing
	 * @param expectedResult The result of the expression
	 */
	public AppTest(MyQueue<String> infix, double expectedResult)
	{
		this.infix = infix;
		this.expectedResult = expectedResult;
	}
	
	@Parameters(name = "{index} plan[{0}]={1}]")
	public static Collection<Object[]> data()
	{		
		String[] infix1 = new String[]{"1","+","2","/","(","(","5","+","6",")",")","*","5","-","1"};
		String[] infix2 = new String[]{"(","7.8","*","2",")"};
		String[] infix3 = new String[]{"9","/","89","*","(","9","-","45",")","+","100"};
		String[] infix4 = new String[]{"57","/","(","(","(","0","+","50.005",")","*","23",")","-","56",")"};
		String[] infix5 = new String[]{"100","*","0","-","100"};
		String[] infix6 = new String[]{"1","+","2","+","3","+","4","+","5","*","100"};
		String[] infix7 = new String[]{"(","100","/","100",")","-","1"};
		String[] infix8 = new String[]{"6.7","*","(","8","-","1",")"};
		String[] infix9 = new String[]{"678","*","239","+","8345","/","556","+","9024","-","-267356"};
		String[] infix10 = new String[]{"(","0.008","*","-76656",")","+","89789","-","-36.56"};
		String[] infix11 = new String[]{"587","*","0","-","50","+","(","0","+","0",")"};
		String[] infix12 = new String[]{"87.356","*","7867.784","/","4"};
		String[] infix13 = new String[]{"9432","*","-902","+","243"};
		String[] infix14 = new String[]{"0","*","3586.8789","+","(","546","-","34","+","(","467","/","7256",")",")"};
		String[] infix15 = new String[]{"79","-","467","-","5467","-","1341"};
		String[] infix16 = new String[]{"(","82.67544","*","3456","/","9856",")"};
		String[] infix17 = new String[]{"3243","+","19284"};
		String[] infix18 = new String[]{"285","/","(","87535.878","-","93454",")","+","100"};
		String[] infix19 = new String[]{"936","*","23","+","368"};
		String[] infix20 = new String[]{"26","+","385","*","(","43","+","64",")"};
		String[] infix21 = new String[]{"1","-","1","+","1"};
		String[] infix22 = new String[]{"432","/","2","+","20"};
		String[] infix23 = new String[]{"83742","-","942183","*","93.324324"};
		String[] infix24 = new String[]{"(","543435","+","432","-","6892","/","9482","*","43.895",")"};
		String[] infix25 = new String[]{"1","+","1"};
		
		return Arrays.asList(new Object[][] {
			{toPostfix(infix1), 0.9},
			{toPostfix(infix2), 15.6},
			{toPostfix(infix3), 96.4},
			{toPostfix(infix4), 0.1},
			{toPostfix(infix5), -100},
			{toPostfix(infix6), 510},
			{toPostfix(infix7), 0},
			{toPostfix(infix8), 46.9},
			{toPostfix(infix9), 438437.0},
			{toPostfix(infix10), 89212.312},
			{toPostfix(infix11), -50},
			{toPostfix(infix12), 171824.5},
			{toPostfix(infix13), -8507421},
			{toPostfix(infix14), 512.1},
			{toPostfix(infix15), -7196},
			{toPostfix(infix16), 28.9},
			{toPostfix(infix17), 22527},
			{toPostfix(infix18), 99.9},
			{toPostfix(infix19), 21896},
			{toPostfix(infix20), 41221},
			{toPostfix(infix21), 1},
			{toPostfix(infix22), 236},
			{toPostfix(infix23), -87844849.6},
			{toPostfix(infix24), 543835.1},
			{toPostfix(infix25), 2}
		});
	}
	
	@Test
	public void testPostfixResult() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		Evaluator e = new Evaluator();
		
		double result = e.calculatePostfix(e.getPostfixFromInfix(infix)).pop();
		
		assertEquals(expectedResult, Math.round(result), 0.5);
	}
	
	/**
	 * Used to get a infix queue from a string array
	 * @param expression The array to take from
	 * @return The infix queue
	 */
	private static MyQueue<String> toPostfix(String[] expression)
	{
		MyQueue<String> postfix = new DynamicArray<String>();
		
		for(int i = 0; i < expression.length; i++)
			postfix.add(expression[i]);
		
		return postfix;
	}
}
