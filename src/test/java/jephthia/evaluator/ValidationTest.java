package jephthia.evaluator;

import org.junit.Test;
import exceptions.DivideByZeroException;
import exceptions.InvalidOperandException;
import exceptions.InvalidParenthesisException;
import exceptions.MisplacedOperandException;
import exceptions.MisplacedOperatorException;
import static org.junit.Assert.fail;

public class ValidationTest
{
	@Test(expected=InvalidParenthesisException.class)
	public void testInvalidParenthesis() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		MyQueue<String> infix = new DynamicArray<String>();

		infix.add("(");
		infix.add("(");
		infix.add("(");
		infix.add("5");
		infix.add("+");
		infix.add("6");
		infix.add(")");
		infix.add("*");
		infix.add("7");
		infix.add(")");
    	
    	Evaluator e = new Evaluator();
    	e.validateInfix(infix);
    	
		fail("This method was supposed to throw an exception but it did not.");
	}
	
	@Test(expected=InvalidOperandException.class)
	public void testInvalidOperand() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		MyQueue<String> infix = new DynamicArray<String>();

		infix.add("(");
		infix.add("-5");
		infix.add("+");
		infix.add("6.54");
		infix.add(")");
		infix.add("*");
		infix.add("7f86#7");
		infix.add("-");
		infix.add("62");
    	
    	Evaluator e = new Evaluator();
    	e.validateInfix(infix);
    	
		fail("This method was supposed to throw an exception but it did not.");
	}
	
	@Test(expected=MisplacedOperatorException.class)
	public void testMisplacedOperator() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		MyQueue<String> infix = new DynamicArray<String>();

		infix.add("4.5");
		infix.add("*");
		infix.add("3");
		infix.add("+");
		infix.add("12.5");
		infix.add("/");
		infix.add("938");
		infix.add("+");
    	
    	Evaluator e = new Evaluator();
    	e.validateInfix(infix);
    	
		fail("This method was supposed to throw an exception but it did not.");
	}
	
	@Test(expected=DivideByZeroException.class)
	public void testDivideByZeroException() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		MyQueue<String> infix = new DynamicArray<String>();

		infix.add("(");
		infix.add("102");
		infix.add("*");
		infix.add("2.98");
		infix.add(")");
		infix.add("/");
		infix.add("0");
    	
    	Evaluator e = new Evaluator();
    	e.validateInfix(infix);
    	
		fail("This method was supposed to throw an exception but it did not.");
	}
	
	@Test(expected=MisplacedOperandException.class)
	public void testMisplacedOperand() throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		MyQueue<String> infix = new DynamicArray<String>();

		infix.add("3.442");
		infix.add("*");
		infix.add("5.324");
		infix.add("+");
		infix.add("(");
		infix.add("98");
		infix.add("-");
		infix.add("6754");
		infix.add("+");
		infix.add("344");
		infix.add("78");
		infix.add(")");
		
    	Evaluator e = new Evaluator();
    	e.validateInfix(infix);
    	
		fail("This method was supposed to throw an exception but it did not.");
	}
}