package exceptions;

public class DivideByZeroException extends Exception
{
	private static final long serialVersionUID = 6466556220376067643L;

	public DivideByZeroException()
	{
		super();
	}

	public DivideByZeroException(String message)
	{
		super(message);
	}
}