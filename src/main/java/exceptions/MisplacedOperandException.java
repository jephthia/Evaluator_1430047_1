package exceptions;

public class MisplacedOperandException extends Exception
{
	private static final long serialVersionUID = -619036892198466334L;

	public MisplacedOperandException()
	{
		super();
	}

	public MisplacedOperandException(String message)
	{
		super(message);
	}
}