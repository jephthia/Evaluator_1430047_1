package exceptions;

public class InvalidParenthesisException extends Exception
{
	private static final long serialVersionUID = 7235711770706561827L;

	public InvalidParenthesisException()
	{
		super();
	}

	public InvalidParenthesisException(String message)
	{
		super(message);
	}
}