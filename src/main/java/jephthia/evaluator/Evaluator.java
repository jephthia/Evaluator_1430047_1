package jephthia.evaluator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import exceptions.DivideByZeroException;
import exceptions.InvalidOperandException;
import exceptions.InvalidParenthesisException;
import exceptions.MisplacedOperandException;
import exceptions.MisplacedOperatorException;

/**
 * This class is used to calculate a mathematical expression
 * by first transforming it into postfix expression and then
 * returning a stack with the result.
 * @author Jephthia Louis
 */
public class Evaluator
{
	private Logger log = LoggerFactory.getLogger(getClass().getName());
	
	/**
	 * Transforms an infix queue to a postfix queue.
	 * @param infix The infix to transform.
	 * @return The postfix queue
	 * @throws InvalidParenthesisException
	 * @throws InvalidOperandException
	 * @throws MisplacedOperatorException
	 * @throws DivideByZeroException
	 * @throws MisplacedOperandException
	 */
	public MyQueue<String> getPostfixFromInfix(MyQueue<String> infix) throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		infix = validateInfix(infix);
		
    	MyStack<String> operandStack = new DynamicArray<String>();
    	
    	MyQueue<String> postfix = new DynamicArray<String>();
    	
    	int size = infix.size();
    	
    	for(int i = 0; i < size; i++)
    	{
    		String op = infix.remove();
    		
    		if(!isOperator(op))
    		{
    			postfix.add(op);
    		}
    		else
    		{
    			boolean retestNewOperand;
    			
    			do
    			{	
    				retestNewOperand = false;
    				
	    			if(operandStack.empty() || "(".equals(op))
	    			{
	    				operandStack.push(op);
	    			}
	    			else if(")".equals(op))
	    			{
	    				//Pop everything off the operator stack until we reach a left parenthesis
	    				while(!operandStack.empty() && !"(".equals(operandStack.peek()))
	    				{
	    					postfix.add(operandStack.pop());
	    				}
	    				
	    				//Remove the left parenthesis
	    				if(!operandStack.empty())
	    					operandStack.pop();
	    			}
	    			else
	    			{
	    				int currOp = getOperatorValue(op);
	    				int prevOp = getOperatorValue(operandStack.peek());
	    				
	    				//Current operator is greater than the previous operator
	    				if(currOp > prevOp)
	    				{
	    					//push to stack
	    					operandStack.push(op);
	    				}
	    				//Current operator is less than the previous operand
	    				else if(currOp <= prevOp)
	    				{
	    					//Switch operators
	    					postfix.add(operandStack.pop());
	    					
	    					//Test the current operator with the new top of the stack
	    					retestNewOperand = true;
	    				}
	    			}
    			}while(retestNewOperand);
    		}
    	}
    	
    	int operatorSize = operandStack.size();
    	
    	for(int h = 0; h < operatorSize; h++)
    		postfix.add(operandStack.pop());
		
    	log.debug("---------------POSTFIX---------------");
    	log.debug(postfix.toString());
    	
		return postfix;
	}
	
	/**
	 * @param postfix The postfix to calculate
	 * @return The stack that contains the final result
	 */
	public MyStack<Double> calculatePostfix(MyQueue<String> postfix)
    {
    	MyStack<Double> tmp = new DynamicArray<Double>();
    	
    	while(!postfix.empty())
    	{
    		if(!isOperator(postfix.element()))
    			tmp.push(Double.parseDouble(postfix.remove()));
    		else
    		{
    			String operator = postfix.remove();
    			Double rightOp = tmp.pop();
    			Double leftOp = tmp.pop();
    			
    			if("*".equals(operator))
    				tmp.push(leftOp * rightOp);
    			else if("/".equals(operator))
    				tmp.push(leftOp / rightOp);
    			else if("+".equals(operator))
    				tmp.push(leftOp + rightOp);
    			else if("-".equals(operator))
    				tmp.push(leftOp - rightOp);
    		}
    	}
    	
    	log.debug("---------------RESULT---------------");
    	log.debug(""+tmp.peek());
    	
    	return tmp;
    }
	
	/**
	 * Validates the given infix
	 * @param infix The infix to validate
	 * @return The same infix
	 * @throws InvalidParenthesisException
	 * @throws InvalidOperandException
	 * @throws MisplacedOperatorException
	 * @throws DivideByZeroException
	 * @throws MisplacedOperandException
	 */
	public MyQueue<String> validateInfix(MyQueue<String> infix) throws InvalidParenthesisException, InvalidOperandException, MisplacedOperatorException, DivideByZeroException, MisplacedOperandException
	{
		System.out.println("--------------------VALIDATING--------------------");
		MyQueue<String> validatedInfix = new DynamicArray<String>();
		
		int leftParenthesisCounter = 0;
		
		String previous = null;
		
		int size = infix.size();
		
		for(int i = 0; i < size; i++)
		{
			String current = infix.remove();
			validatedInfix.add(current);
			
			System.out.println("pre " + previous + " curr " + current);
		
			// Increase the opening parenthesis counter
			if("(".equals(current))
			{
				leftParenthesisCounter++;
			}
			else if(")".equals(current))
			{
				// Decrease the left parenthesis counter and
				// make sure that the counter is not negative
				// if it is, that means that this closing parenthesis
				// is extra.
				leftParenthesisCounter--;
				
				if(leftParenthesisCounter < 0)
					throw new InvalidParenthesisException();
			}
			else if(isOperator(current))
			{
				// An operator is only valid if it is
				// after an operand or a closing parenthesis
				// so if the previous was anything else
				// throw an exception
				if(previous == null || "(".equals(previous) || isOperator(previous, false))
					throw new MisplacedOperatorException();
			}
			else
			{
				// At this point the only other valid possibility
				// is for the current string to be an operand,
				// so if it's not throw an exception.
				if(!isDecimal(current))
					throw new InvalidOperandException();
				
				// If the current operand is a number,
				// check if it is zero and then check
				// if operation is a division.
				if("/".equals(previous) && Double.parseDouble(current) == 0)
					throw new DivideByZeroException();
				
				// An operand is valid if it is after
				// an operator or an opening parenthesis
				// so if the previous was anything else
				// throw an exception
				if(!(previous == null || "(".equals(previous) || isOperator(previous, false)))
					throw new MisplacedOperandException();
			}
			
			previous = current;
		}
		
		// After going through the whole infix, the left
		// parenthesis counter has to be 0 otherwise
		// an extra parenthesis was left.
		if(leftParenthesisCounter != 0)
			throw new InvalidParenthesisException();
		
		// If the last value of the infix is an operator
		// throw an exception.
		if(isOperator(previous, false))
			throw new MisplacedOperatorException();
    	
    	return validatedInfix;
	}
	
	/**
	 * @return true if the string is a number, false otherwise
	 */
	private boolean isDecimal(String value)
	{
		try
		{
			Double.parseDouble(value);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}
	
	/**
	 * @return 1 if * or /, 0 if + or -, and -1 for anything else
	 */
	private int getOperatorValue(String value)
	{
		if("*".equals(value) || "/".equals(value))
			return 1;
		else if("+".equals(value) || "-".equals(value))
			return 0;
		
		return -1;
	}
	    
	/**
	 * @return true if the string is *,/,+,-,(,) false otherwise
	 */
    private boolean isOperator(String value)
    {
    	return isOperator(value, true);
    }
    
    /**
	 * @return true if the string is *,/,+,- false otherwise
	 */
    private boolean isOperator(String value, boolean matchParenthesis)
    {
    	if(value == null)
    		return false;
    	
    	String operators = matchParenthesis ? "*/+-()" : "*/+-";
    	
    	return operators.indexOf(value.trim()) != -1;
    }
}
