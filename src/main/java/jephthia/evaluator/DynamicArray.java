package jephthia.evaluator;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * This class is used to managed a queue or
 * stack as a dynamic array.
 * @author Jephthia Louis
 *
 * @param <T> The type of this dynamic array.
 */
public class DynamicArray<T> implements MyStack<T>, MyQueue<T>
{
	private T[] arr;
	private int capacity;
	private int backPointer;
	private int frontPointer;
	private int growth;
	
	/**
	 * Creates a new dynamic array
	 */
	public DynamicArray()
	{
		this(10);
	}
	
	/**
	 * Creates a new dynamic array
	 * @param capacity The total capacity of the array.
	 */
	public DynamicArray(int capacity)
	{
		this(capacity, 2);
	}
	
	/**
	 * Creates a new dynamic array
	 * @param capacity The total capacity of the array.
	 * @param growth The growth factor to use when resizing the array.
	 */
	public DynamicArray(int capacity, int growth)
	{
		this.capacity = capacity;
		this.frontPointer = 0;
		this.backPointer = 0;
		this.growth = growth;
		
		arr = (T[])new Object[capacity];
	}
	
	/**
	 * @param t The element to add to this dynamic array
	 */
	public void add(T t)
	{
		if(size() == capacity)
			arr = Arrays.copyOf(arr, capacity * growth);
		
		arr[backPointer++] = t;
	}
	
	/**
	 * Add a new element to the array
	 * @param pos The position where to add the element
	 * @param t The element to add
	 */
	public void add(int pos, T t)
	{
		if(pos < 0 || pos >= size())
			throw new IllegalArgumentException();
		
		T tmp = t;
		
		for(int i = 0; i < arr.length; i++)
		{
			if(i == pos)
			{
				tmp = arr[i];
				arr[i] = t;
			}
			else if(i > pos)
			{
				T curr = arr[i];
				arr[i] = tmp;
				tmp = curr;
			}
		}
	}

	/**
	 * Removes the element at the top
	 * @return The element at the top
	 */
	public T remove()
	{
		if(empty())
			throw new EmptyStackException();
		
		return arr[frontPointer++];
	}
	
	/**
	 * Remove an element from the array
	 * @param pos The position of the element to remove
	 * @return The removed element
	 */
	public T remove(int pos)
	{
		if(pos < 0 || pos >= size())
			throw new IllegalArgumentException();
		
		T removed = arr[pos];
		
		for(int i = 0; i < arr.length; i++)
		{
			if(i >= pos && (i + 1) < arr.length)
			{
				arr[i] = arr[i + 1];
			}
		}
		
		return removed;
	}
	
	/**
	 * Replaces an element at the specified position.
	 * @param pos The position of the element to replace
	 * @param t The new element
	 */
	public void set(int pos, T t)
	{
		if(pos < 0 || pos >= size())
			throw new IllegalArgumentException();
		
		arr[pos] = t;
	}

	/**
	 * @return The element at the top without removing it.
	 */
	public T peek()
	{
		if(empty())
			throw new EmptyStackException();
		
		return arr[backPointer - 1];
	}

	/**
	 * @param t The element to push on this dynamic array
	 */
	public void push(T t)
	{
		if(size() == capacity)
			arr = Arrays.copyOf(arr, capacity * growth);
		
		arr[backPointer++] = t;
	}

	/**
	 * Removes the element at the top
	 * @return The element at the top
	 */
	public T pop()
	{
		if(size() <= 0)
			throw new EmptyStackException();
		
		return arr[--backPointer];
	}

	/**
	 * @return The size of the dynamic array.
	 */
	public int size()
	{
		return backPointer - frontPointer;
	}

	/**
	 * @return The element at the top without removing it.
	 */
	public T element()
	{
		if(empty())
			throw new EmptyStackException();		
		
		return arr[frontPointer];
	}
	
	/**
	 * Retrieves the element at the specified position.
	 * @param pos The position of the element to retrieve.
	 * @return The element at the specified position.
	 * @throws IllegalArgumentException
	 */
	public T get(int pos)
	{
		if(pos < 0 || pos >= size())
			throw new IllegalArgumentException();
		
		return arr[pos];
	}
	
	/**
	 * @return true if the this dynamic array is empty, false otherwise.
	 */
	public boolean empty()
	{
		return size() == 0;
	}
	
	/**
	 * Prints out all the members of this dynamic array.
	 */
	@Override public String toString()
	{
		String str = "";
		
		for(int i = 0; i < arr.length; i++)
		{
			// Don't print nulls
			if(arr[i] == null)
				continue;
			
			str += arr[i] + " ";
		}
		
		return str;
	}
}
