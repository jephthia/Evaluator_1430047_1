package jephthia.evaluator;

public interface MyQueue<T>
{
	public void add(T t);
	public T remove();
	public T element();
	public int size();
	public boolean empty();
}
